# [2.0.0](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.8.0...v2.0.0) (2021-09-15)


### Features

* **sa:** sasada ([576117c](https://gitlab.com/hakanhueriyet/testsemantic/commit/576117c0a1848df7477b6ef2bcd2a36938234e23)), closes [#CA-273](https://gitlab.com/hakanhueriyet/testsemantic/issues/CA-273)


### BREAKING CHANGES

* **sa:** My hart

# [1.8.0](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.7.1...v1.8.0) (2021-09-15)


### Features

* **as:** sa ([ddc0877](https://gitlab.com/hakanhueriyet/testsemantic/commit/ddc087764716485781f8297fcafc7d05a0edd21f))
* sa ([c3eb4ba](https://gitlab.com/hakanhueriyet/testsemantic/commit/c3eb4babd4f80a960e84f8841c7f4c08309afd1e))
* **asa:** asa ([4adc482](https://gitlab.com/hakanhueriyet/testsemantic/commit/4adc482674e9e2921170a9eb3b833c74ff7a670f))
* **asa:** asa ([7f282f8](https://gitlab.com/hakanhueriyet/testsemantic/commit/7f282f8db6f57c5afed4945c0b0e715e6168409c))
* **ayak:** got feat ([fc2df64](https://gitlab.com/hakanhueriyet/testsemantic/commit/fc2df64bfc567d29808a7b9970f92156f74cae1d))
* **package:** constantin ([4f42ac0](https://gitlab.com/hakanhueriyet/testsemantic/commit/4f42ac029553adf820a32687f36cc7e613586b46))

## [1.7.1](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.7.0...v1.7.1) (2021-09-15)


### Bug Fixes

* **rleease:** release asa ([d2f5003](https://gitlab.com/hakanhueriyet/testsemantic/commit/d2f5003a8fcc870c726b188726d5897056b81644))

## [1.5.2](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.5.1...v1.5.2) (2021-09-15)


### Bug Fixes

* **gitlab:** oww ([435dc3d](https://gitlab.com/hakanhueriyet/testsemantic/commit/435dc3d698f97c48a9f033f2d395344d0b764354))

## [1.5.1](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.5.0...v1.5.1) (2021-09-15)


### Bug Fixes

* **bump:** bum vers added ([0faeac2](https://gitlab.com/hakanhueriyet/testsemantic/commit/0faeac2cf0f6f57061ec98a8424a4e12ea8fe37b))

# [1.5.0](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.4.0...v1.5.0) (2021-09-15)


### Features

* **file:** new libss ([adefaed](https://gitlab.com/hakanhueriyet/testsemantic/commit/adefaed3d8288f5c78172535001af39e614dfa24))

## [1.3.1](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.3.0...v1.3.1) (2021-09-14)


### Bug Fixes

* **ci:** skip removed ([891c4a1](https://gitlab.com/hakanhueriyet/testsemantic/commit/891c4a1b4ee4325b91539650b5b052ede4a27741))

# [1.3.0](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.2.0...v1.3.0) (2021-09-14)


### Bug Fixes

* **md:** changelog removed ([4ad9534](https://gitlab.com/hakanhueriyet/testsemantic/commit/4ad953409c5d4e7de85020c438a6bcaa64340a67))
* **test:** oskoribk ([b6e1cf4](https://gitlab.com/hakanhueriyet/testsemantic/commit/b6e1cf4f6609b9dc3e7aa5809d8293e710b644cf))


### Features

* **file:** oskorbik changed ([516c1a2](https://gitlab.com/hakanhueriyet/testsemantic/commit/516c1a2d5ba439c5514b00467dbda3b2c2644e45))

# [1.2.0](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.1.0...v1.2.0) (2021-09-14)


### Bug Fixes

* **release:** fixxit ([fe773e0](https://gitlab.com/hakanhueriyet/testsemantic/commit/fe773e028c944cd1d776e9c24b367ad835e91a1a))
* **sas:** asasa ([d3624bd](https://gitlab.com/hakanhueriyet/testsemantic/commit/d3624bdaaac0d9bc1064effd6d97154f50c24359))
* **ssa:** new versionakis ([729ada3](https://gitlab.com/hakanhueriyet/testsemantic/commit/729ada3c988e048cbec28f243318a1c7fc74b722))


### Features

* **fut:** feat ([768b28d](https://gitlab.com/hakanhueriyet/testsemantic/commit/768b28d4ec5fb95472f7844d7d3d81fd0be8bade))

# [1.1.0-development.5](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.1.0-development.4...v1.1.0-development.5) (2021-09-14)


### Bug Fixes

* **release:** fixxit ([fe773e0](https://gitlab.com/hakanhueriyet/testsemantic/commit/fe773e028c944cd1d776e9c24b367ad835e91a1a))
* **sas:** asasa ([d3624bd](https://gitlab.com/hakanhueriyet/testsemantic/commit/d3624bdaaac0d9bc1064effd6d97154f50c24359))


### Features

* **fut:** feat ([768b28d](https://gitlab.com/hakanhueriyet/testsemantic/commit/768b28d4ec5fb95472f7844d7d3d81fd0be8bade))

# [1.1.0-development.4](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.1.0-development.3...v1.1.0-development.4) (2021-09-14)


### Bug Fixes

* **ssa:** new versionakis ([729ada3](https://gitlab.com/hakanhueriyet/testsemantic/commit/729ada3c988e048cbec28f243318a1c7fc74b722))

# [1.1.0-development.2](https://gitlab.com/hakanhueriyet/testsemantic/compare/v1.1.0-development.1...v1.1.0-development.2) (2021-09-14)


### Bug Fixes

* **release:** asasasa ([2f485b5](https://gitlab.com/hakanhueriyet/testsemantic/commit/2f485b51ec4cc48364c868f3e0b0d3a24e186cc2))
